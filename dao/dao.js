var mongoose = require('mongoose'), schemas;




if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
   mongoose.connect(process.env.OPENSHIFT_MONGODB_DB_URL + "/example"); 
} else {
   mongoose.connect('mongodb://127.0.0.1/brotherhood'); 
}
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

var moment = require("moment");

db.once('open', function callback() {
    console.log("DB Connection active!");
    schemas = require("./schema.js")(mongoose);
});

var todo = {
    save: function(data, res) {
      
        var todo = new schemas.todo({
            icon: data.icon,
            description: data.description,
            name: data.name,
            date: data.date
        });
        
        todo.save(function(err, todo, affected) {
            var result = err ? false: true;
            res.send({result : result});
        });       
        
    },
    all: function(callback) {
          schemas.todo.find({}, function(err, todos) {
            callback(todos);
        });
    },
    remove: function(id, res) {
          schemas.todo.remove({_id : id}, function(err, todo, affected) {
            var result = err ? false: true;
            res.send({result : result});
        });
    }
};


exports.todo = todo;
