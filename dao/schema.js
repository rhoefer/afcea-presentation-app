module.exports = function(mongoose) {

    var todoSchema = mongoose.Schema({
        icon: String,
        description: String,
        name: String,
        priority: Number
    });

    var todoModel = mongoose.model("todo", todoSchema);
    
    console.log("Finished initializing schemas");

    return {
        todo: todoModel
    }
}