var express = require('express');
var fs = require('fs');
var app = express();

var bodyParser = require("body-parser");
var dao = require("./dao/dao");


var server_port = process.env.OPENSHIFT_NODEJS_PORT || 3000
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || 'localhost'


// Set EJS as our template engine.
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// Add a static route for all public files
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res){
   dao.todo.all(function(todos) {
      res.render("index.ejs", {"todos" : todos});
   })
});

app.post('/save', function(req, res) {   
   dao.todo.save(req.body, res);
});

app.post('/remove', function(req, res) {   
   dao.todo.remove(req.body.id, res);
});

app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + server_port );
});
